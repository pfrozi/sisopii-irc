all: sirc_server sirc_client

sirc_client: libutil-client
	@gcc -g -o bin/sirc-client src/main_cli.c -pthread -Llib -lutilclient -Wall -lrt -lncurses

sirc_server: libutil-server
	@gcc -g -o bin/sirc-server src/main_srv.c -pthread -Llib -lutilserver -Wall -lrt 

libutil-server: sirc_server.o sirc_util.o list_s.o lock_fifo.o
	@ar crs lib/libutilserver.a bin/sirc_server.o bin/sirc_util.o bin/list_s.o bin/lock_fifo.o

libutil-client: sirc_client.o sirc_util.o list_s.o ui.o
	@ar crs lib/libutilclient.a bin/sirc_client.o bin/sirc_util.o bin/list_s.o bin/ui.o

sirc_server.o: src/sirc_server.c sirc_util.o list_s.o lock_fifo.o
	@gcc -c src/sirc_server.c -o bin/sirc_server.o

sirc_client.o: src/sirc_client.c sirc_util.o
	@gcc -c src/sirc_client.c -o bin/sirc_client.o

ui.o: src/ui.c sirc_util.o
	@gcc -c src/ui.c -o bin/ui.o 

sirc_util.o: src/sirc_util.c
	@gcc -c src/sirc_util.c -o bin/sirc_util.o

lock_fifo.o: src/lock_fifo.c
	@gcc -c src/lock_fifo.c -o bin/lock_fifo.o

list_s.o: src/list_s.c
	@gcc -c src/list_s.c -o bin/list_s.o

clean:
	@rm -rf bin/*.o
	@rm -rf bin/sirc-client
	@rm -rf bin/sirc-server
	@rm -rf lib/*.a



