/**
 * UI, the user interface of the chat
 */

#ifndef UI_H
#define UI_H

#include "../include/sirc_util.h"
#include <ncurses.h>
#include <time.h>
#include <string.h>

#define MOUTH_HEIGHT 4
#define DEFAULT_USERNAME "Anonymous"

#define DEFAULT_COLOR 1

typedef struct ui {
    WINDOW *ear_window;
    WINDOW *ear_box;

    WINDOW *mouth_window;
    WINDOW *mouth_box;

    char username[MAX_NAME_LEN_BUFF];
} Ui;

/**
 * Needs to be called before any other UI function. Starts the UI system and
 * creates a mouth window and an ear window on the screen.
 */
void
ui_start(Ui *ui);

/**
 * Destroys the windows and finishes the UI system.
 */
void
ui_finish(Ui *ui);

/**
 * Writes the message contained in the protocol on the ear window alongside with
 * its timestamp and the username of its sender.
 */
void
ui_listen(Ui *ui, const protocol_t protocol);

/**
 * Allows a message to be written on the mouth screen and puts it into the protocol,
 * but doesn't set the timestamp nor the username of the sender.
 */
void
ui_speak(Ui *ui, protocol_t *protocol);

/**
 * Clears the ear window from all the messages written on it.
 */
void
ui_show_room(Ui *ui);

#endif
