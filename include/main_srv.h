#ifndef MAIN_SRV_H
#define MAIN_SRV_H

#include <stdio.h>
#include <stdlib.h>
#include "sirc_server.h"

/*
    CONSTS
*/

#define MAIN_SRV_DEBUG     1


#define RETURN_NOTHING     1
#define ERROR_INVALID_ARGS 2
#define ERROR_INVALID_PORT 3
#define ERROR_INIT_SERVER  4
#define RETURN_SUCCESS     0

#define ARGS_MAX           3

#define ARGS_HELP        "-h"  // Return a help text
#define ARGS_PORT_NUMBER "-p"  // Port number which will be used

/*
    Text ERRORS
*/
#define ERR_PORT_NUMBER   "Numero da porta invalida!"
#define ERR_PORT_RESERVED "Numero da porta escolhida eh reservada!"

void help();

#endif
