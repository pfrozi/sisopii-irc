#ifndef MAIN_CLI_H
#define MAIN_CLI_H

#include <stdio.h>
#include <stdlib.h>
#include "sirc_client.h"

/*
    CONSTS
*/

#define MAIN_CLI_DEBUG     1


#define RETURN_NOTHING     1
#define ERROR_INVALID_ARGS 2
#define ERROR_INVALID_PORT 3
#define ERROR_INVALID_HOST 4
#define ERROR_INIT_CLIENT  5
#define RETURN_SUCCESS     0

#define ARGS_MAX           7

#define ARGS_HELP        "-h"  // Return a help text
#define ARGS_HOST_NUMBER "-s"  // Host addr used
#define ARGS_PORT_NUMBER "-p"  // Host port number used
#define ARGS_PCLI_NUMBER "-c"  // Port number of client of which will be used

/*
    Text ERRORS
*/
#define ERR_HOST_NUMBER   "Endereço do servidor inválido invalida!"
#define ERR_PORT_NUMBER   "Numero da porta do host invalida!"
#define ERR_CPORT_NUMBER  "Numero da porta do cliente invalida!"
#define ERR_PORT_RESERVED "Numero da porta escolhida eh reservada!"

void help();

#endif
