#ifndef SIRC_UTIL_H
#define SIRC_UTIL_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define	SOCKET	int
#define INVALID_SOCKET  ((SOCKET)~0)

#define SIRC_UTIL_DEBUG             1

#define MAX_PACKET                  2048           // buffer of message
#define MAX_NAME_LEN                20             // string of name
#define MAX_NAME_LEN_BUFF           MAX_NAME_LEN+1 // buffer of name
#define DEFAULT_PORT                2023           // DEFAULT port

#define CONCURRENT_CONNECTIONS      256


#define SCK_INIT_SUCCESS            0
#define SCK_ERR_CREATE              1
#define SCK_ERR_BIND                2
#define SCK_ERR_LISTEN              3
#define SCK_ERR_CONNECT             4

#define SCK_ACCEPT_SUCCESS          0
#define SCK_ACCEPT_ERROR            1

#define PING                        "\\ping"
#define PONG                        "pong!"

#define SEND_OK                     0
#define SEND_ERROR                  1

#define RECV_OK                     0
#define RECV_ERROR                  1

#define S_TO_C_COMM_NULL            0
#define S_TO_C_COMM_NICK            1

#define MESSAGE_BUFF_SIZE           MAX_PACKET-sizeof(struct tm)-MAX_NAME_LEN_BUFF-sizeof(int)

typedef struct sck{

    SOCKET   socket;
    int      port;
    struct   sockaddr_in addr;

}sck_t;

typedef struct protocol{

    int         s_to_c_command;
    
    struct      tm date_time;
    char        user[MAX_NAME_LEN_BUFF];
    char        message[MESSAGE_BUFF_SIZE];
    
}protocol_t;

typedef struct client_sck{

    sck_t*      sck;
    struct      sockaddr_in addr_server;
    char        buffer[MAX_PACKET];

}client_sck_t;


int    init_server_sck(sck_t* sck, int port);
int    accept_sck(sck_t* sck, sck_t* sck_ret);

int    init_client_sck(sck_t* sck_client, int port);
int    init_client_to_server(client_sck_t* sck_client, char* host, int port);

protocol_t  create_message(char* user, char* message);
protocol_t  create_message_comm(char* user, char* message, int command);

int         send_message(SOCKET socket, protocol_t message);
int         receive_message(SOCKET socket, protocol_t *message);
#endif
