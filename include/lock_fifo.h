#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

typedef struct lock_fifo {
    
    pthread_cond_t      cond;
    pthread_mutex_t     mutex;
    unsigned long       queue_head, queue_tail;
    
} lock_fifo_t;

void lock_fifo_init(lock_fifo_t* locker);
void lock_fifo_lock(lock_fifo_t *lock);
void lock_fifo_unlock(lock_fifo_t *lock);