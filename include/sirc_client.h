#ifndef SIRC_CLIENT_H
#define SIRC_CLIENT_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include "sirc_util.h"
#include "ui.h"

#define SIRC_CLIENT_DEBUG     1

#define INIT_SUCCESS          0
#define INIT_ERROR            1

#define LEAVE_COMMAND         "\\leave"
#define EXIT_COMMAND          "\\exit"

int   init_client(char* host, int host_port, int client_port);
void  get_name();
char* get_message();  


void *receiver();
void *sender();
#endif
