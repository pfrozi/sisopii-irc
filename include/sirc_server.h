#include <time.h>
#include <pthread.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include "sirc_util.h"
#include "list_s.h"
#include "sirc_commands.h"


#define NICK_SERVER           "server"

#define SIRC_SERVER_DEBUG     1

#define INIT_SUCCESS          0
#define INIT_ERROR            1

#define CREATE_USER_SUCCESS   0
#define CREATE_USER_ERROR     1

#define POSTMAN_LIVE          0
#define POSTMAN_DEAD_ERR_REC  1
#define POSTMAN_DEAD_ERR_SND  2
#define POSTMAN_DEAD_EXIT     3

#define SIRC_COMMAND_IDENT    '\\'

#define SLEEP_GRAVEDIGGER     500
#define SLEEP_WORKER          50

typedef struct irc_server{
    
    sck_t*      sck;
    list_s_t*   rooms;
    list_s_t*   postmen;
    
}irc_server_t;

typedef struct irc_room{
    
    char        name[MAX_NAME_LEN_BUFF];
    bool        restrict;
    
    struct tm   time_last_msg;           // referring to the time server clock
}irc_room_t;

typedef struct irc_user{
    
    int         id_user;
    char        nick[MAX_NAME_LEN_BUFF];
    
    irc_room_t* room;
    
}irc_user_t;

typedef struct postman{
    
    sck_t*         sck;
    irc_server_t*  irc_server;
    pthread_t*     thread_postman;
    
    int            id_user;
    irc_user_t*    user;
    
    int            status;
    
}postman_t;




int   init_server(int port);
int   init_worker(irc_server_t* server);

postman_t* create_postman(irc_server_t* server, postman_t* new_postman);
int create_user(postman_t* postman);

void *run_postman(postman_t* postman);

void process_message(postman_t* postman, protocol_t proto);
void server_send(postman_t* postman, protocol_t proto);

void *run_gravedigger(irc_server_t* server);

int create_room(irc_server_t* s , char* name);

/*
#########################################
    COMMANDS
#########################################
*/    

int comm_read_command(postman_t* postman, char* message);

int comm_create_user(postman_t* postman, char* nick);
int comm_create_room(postman_t* postman, char* name);

int comm_join_room(postman_t* postman, char* name);
int comm_leave_room(postman_t* postman, char* name);
int comm_alter_nick(postman_t* postman, char* nick);

int comm_send_broadcast_room(postman_t* postman, protocol_t proto);
int comm_send_broadcast(postman_t* postman, protocol_t proto);
int comm_all(postman_t* postman, protocol_t proto);

int comm_ping(postman_t* postman);
int comm_exit(postman_t* postman);

int comm_list_users(postman_t* postman);
int comm_list_roms(postman_t* postman);
