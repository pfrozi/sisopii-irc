#ifndef LIST_S_H
#define LIST_S_H

#include <stdlib.h>
#include <stdio.h>
#include "lock_fifo.h"

#define LIST_S_DEBUG            1

#define LIST_DESTROY_SUCCESS    0
#define LIST_DESTROY_ERR_NULL   1

#define LIST_ADD_SUCCESS        0

#define LIST_REMOVE_SUCCESS     0
#define LIST_REMOVE_OUT_RANGE   1
#define LIST_REMOVE_LST_EMPTY   2

#define LIST_GET_SUCESS         0
#define LIST_GET_OUT_RANGE      1
#define LIST_GET_LST_EMPTY      2

typedef struct list_s{

    void*          item;
    struct list_s* next;
    
    lock_fifo_t lock;

}list_s_t;


void list_init(list_s_t* list);
int list_destroy(list_s_t* list);

int list_add_head(list_s_t* list, void* item);        //stack
int list_add_tail(list_s_t* list, void* item);        //FIFO

list_s_t* list_remove(list_s_t* list, void* item, int* ret);
    
int   list_get_first(list_s_t* list, void* item);
void* list_get_first2(list_s_t* list);

int list_get_item(list_s_t* list, int index, void* item);


#endif
