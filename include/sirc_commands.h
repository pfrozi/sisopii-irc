#ifndef SIRC_COMMANDS_H
#define SIRC_COMMANDS_H

#define SIRC_COMM_CREATE_USER       "create_user"
#define SIRC_COMM_CREATE_ROOM       "create_room"

#define SIRC_COMM_JOIN_ROOM         "join"
#define SIRC_COMM_LEAVE_ROOM        "leave"
#define SIRC_COMM_ALTER_NICK        "nick"
#define SIRC_COMM_PING              "ping"

#define SIRC_COMM_ALL               "all"
#define SIRC_COMM_EXIT              "exit"

#define SIRC_COMM_LIST_USERS        "listu"
#define SIRC_COMM_LIST_ROOMS        "listr"

#define SIRC_FORMAT_LST             "%.25s\t%.25s\n"
#define SIRC_FORMAT_LST2            "%.25s\t%d\n"

#define READ_COMM_SUCCESS           0
#define ERR_READ_COMM_INVALID       1
#define ERR_READ_COMM_ARGS_INVALID  2
#define ERR_READ_COMM_ENTRY_TOO_BIG 3

#define CREATE_USER_SUCCESS         0
#define ERR_CREATE_USER_EXISTS      1
#define ERR_CREATE_USER_INVALID     2
#define ERR_CREATE_USER_LENGTH      3

#define CREATE_ROOM_SUCESS          0
#define ERR_CREATE_ROOM_EXISTS      1
#define ERR_CREATE_ROOM_INVALID     2
#define ERR_CREATE_ROOM_LENGTH      3

#define JOIN_SUCESS                 0
#define ERR_JOIN_NOT_EXISTS         1
#define ERR_JOIN_EXISTS_USER        2

#define LEAVE_SUCESS                0
#define ERR_LEAVE_NOT_EXISTS        1
#define ERR_LEAVE_ARENT_ROOM        2
#define ERR_LEAVE_ERROR             3

#define ALTER_NICK_SUCESS           0
#define ERR_ALTER_NICK_ALRDY_EXISTS 1
#define ERR_ALTER_NICK_INVALID      2
#define ERR_ALTER_NICK_LENGTH       3
#define ERR_ALTER_NICK              4

#define EXIT_SUCCESS                0
#define EXIT_ERROR                  1

#define SEND_BROAD_SUCESS           0
#define ERR_SEND_BROAD              1

#define ERR_SEND_LISTU              0
#define SEND_LISTU_OK               1

#define ERR_SEND_LISTR              0
#define SEND_LISTR_OK               1

#define ERR_COMM_SEND               255

#define MAX_ENTRY 					32



#endif
