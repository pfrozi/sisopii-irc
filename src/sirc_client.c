#include "../include/sirc_client.h"

static char* my_name = NULL;
static client_sck_t* client = NULL;
static pthread_t* tsender   = NULL;
static pthread_t* treceiver = NULL;

static Ui face;
static bool play=true;

int init_client(char* host, int host_port, int client_port){

    #if(SIRC_CLIENT_DEBUG)
    printf("sirc_client: init_client(host = %s, host_port = %d, client_port = %d)\n", host, host_port, client_port);
    #endif

    tsender = (pthread_t*)malloc(sizeof(pthread_t));
    treceiver = (pthread_t*)malloc(sizeof(pthread_t));

    client = (client_sck_t*)malloc(sizeof(client_sck_t));
    client->sck       = (sck_t*)malloc(sizeof(sck_t));

    switch(init_client_sck(client->sck, client_port)){

        case SCK_ERR_CREATE:{
            printf("Erro ao criar socket!\n");
            return INIT_ERROR;
        }
        case SCK_ERR_BIND:{
            printf("Erro ao realizar bind()!\n");
            return INIT_ERROR;
        }
    }

    #if(SIRC_CLIENT_DEBUG)
    printf("sirc_client: init_client_sck(&sck = %p, port = %d) ok!\n", client->sck, client_port);
    #endif

    switch(init_client_to_server(client, host, host_port)){

        case SCK_ERR_CONNECT:{
            printf("Erro ao tentar conectar ao servidor!(host: %s, port: %d)\n",host, host_port);
            return INIT_ERROR;
        }
    }

    #if(SIRC_CLIENT_DEBUG)
    printf("sirc_client: init_client_to_server(&client = %p, host = %s, host_port = %d) ok!\n"
           , client, host, host_port);
    #endif

    ui_start(&face);
    ui_show_room(&face);

    pthread_create(tsender, NULL,(void*)sender, NULL);
    pthread_create(treceiver, NULL,(void*)receiver, NULL);

    while(play){
        usleep(5000*1000);
    }

    ui_finish(&face);
    printf("Client encerrado!\n");

    return INIT_SUCCESS;
}

void get_name(){

    char name[MAX_NAME_LEN_BUFF+256];


    printf("Escolha seu nick (Max. %d carac): " , MAX_NAME_LEN);
    fgets(name, MAX_NAME_LEN_BUFF+256, stdin);

    while(strlen(name)>MAX_NAME_LEN){
        printf("O nick escolhido possui mais de %d caracteres!\n" , MAX_NAME_LEN);
        printf("Escolha seu nick (Max. %d carac): " , MAX_NAME_LEN);
        fgets(name, MAX_NAME_LEN_BUFF+256, stdin);
    }

    if(my_name!=NULL)   free(my_name);

    my_name = (char*)malloc(sizeof(char)*MAX_NAME_LEN_BUFF);

    memset(my_name, '\0', MAX_NAME_LEN_BUFF);
    strncpy(my_name, name, strlen(name)-1);
}

char* get_message(){

    char* msg  = (char*) malloc(sizeof(char)*MESSAGE_BUFF_SIZE);

    printf("\t> " );
    fgets(msg, MESSAGE_BUFF_SIZE, stdin);

    while(strlen(msg)>MESSAGE_BUFF_SIZE){
        printf("Mensagem muito longa! (tamanho max %lu)\n" , MESSAGE_BUFF_SIZE);
        printf("\t> " );
        fgets(msg, MESSAGE_BUFF_SIZE, stdin);
    }

    return msg;
}

void *receiver(){

    while(play){

        protocol_t p_r;

        if(receive_message(client->sck->socket, &p_r)==SEND_ERROR){
            printf("Erro ao receber mensagem!\n");

        }else{

            if(p_r.s_to_c_command == S_TO_C_COMM_NICK){     // server say: Ok, you can change your nick
                strncpy(face.username, p_r.message, MAX_NAME_LEN_BUFF);
                continue;
            }

            ui_listen(&face, p_r);
        }

        usleep(100);
    }

}

void *sender(){

    protocol_t p;

    while(play){

        ui_speak(&face, &p);

        if(send_message(client->sck->socket, p) == SEND_ERROR){
            printf("Erro ao enviar mensagem!\n");
        }

        if(strcmp(p.message, EXIT_COMMAND)==0){
            //ui_finish(&face); segfault was happening because of that
            play = false;
            break;
        }
    }
}
