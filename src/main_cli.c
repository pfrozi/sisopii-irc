#include "../include/main_cli.h"

void help(){


    printf("\n _____ _____ _____  ___________ _____ _____ _____         ");
    printf("\n/  ___|_   _/  ___||  _  | ___ \\_   _|_   _|_   _|        ");
    printf("\n\\ `--.  | | \\ `--. | | | | |_/ / | |   | |   | | _ __ ___ ");
    printf("\n `--. \\ | |  `--. \\| | | |  __/  | |   | |   | || '__/ __|");
    printf("\n/\\__/ /_| |_/\\__/ /\\ \\_/ / |    _| |_ _| |_ _| || | | (__ ");
    printf("\n\\____/ \\___/\\____/  \\___/\\_|    \\___/ \\___/ \\___/_|  \\___|");
    printf("\n");
    printf("\n");
    printf("\nTrabalho de Sistemas Operacionais II");
    printf("\nAplicativo de Chat utilizando sockets - SISOPIIirc");
    printf("\n");
    printf("\nsirc-server");
    printf("\n\t-h\t, Ajuda");
    printf("\n\t-p\t, Numero da porta do servidor");
    printf("\n");
    printf("\nsirc-client");
    printf("\n\t-h\t, Ajuda");
    printf("\n\t-s\t, IP do servidor");
    printf("\n\t-p\t, Porta do servidor");
    printf("\n\t-c\t, Porta do cliente");
    printf("\n");
    printf("\n");
}

int main(int argc, char **argv) {
    
    #if(MAIN_CLI_DEBUG)
    printf("main_cli: main() init\n");
    #endif
    int    client_port = -1;
    int    host_port = -1;
    char*  host;
    
    
    if (argc < ARGS_MAX || (argc==1 && !(strcmp(argv[1],ARGS_HELP)==0) )) {
        
        printf("Argumentos invalidos!\n");
        
        help();
        return (ERROR_INVALID_ARGS);

    } else {
        
        int i;
        
        for (i = 1; i < argc; i+=2) {

            if (i + 1 != argc)
            {
                char* arg = argv[i+1];

                if (strcmp(argv[i],ARGS_HELP) == 0) {

                    help();
                    return (RETURN_NOTHING);

                } else if (strcmp(argv[i], ARGS_PORT_NUMBER)==0) {

                    host_port = atoi(arg);

                } else if (strcmp(argv[i], ARGS_PCLI_NUMBER)==0) {

                    client_port = atoi(arg);

                } else if (strcmp(argv[i], ARGS_HOST_NUMBER)==0) {

                    host = arg;

                }else {
                    
                    printf("Argumentos invalidos!\n");
                    return (ERROR_INVALID_ARGS);

                }
            }
        }
    }

    
    if(!(host_port>=0 && host_port<=65535)){
    
        printf("ERR: %s (port %d)\n", ERR_PORT_NUMBER, host_port);
        return(ERROR_INVALID_PORT);
        
    } else if(host_port>=0 && host_port<=1023){
    
        printf("ERR: %s (port %d)\n", ERR_PORT_RESERVED, host_port);
        return(ERROR_INVALID_PORT);
        
    }
    if(!(client_port>=0 && client_port<=65535)){
    
        printf("ERR: %s (port %d)\n", ERR_PORT_NUMBER, client_port);
        return(ERROR_INVALID_PORT);
        
    } else if(client_port>=0 && client_port<=1023){
    
        printf("ERR: %s (port %d)\n", ERR_CPORT_NUMBER, client_port);
        return(ERROR_INVALID_PORT);
        
    }
    
    #if(MAIN_CLI_DEBUG)
    printf("main_cli: main() - selected host %s\n", host);
    printf("main_cli: main() - selected host port %d\n", host_port);
    printf("main_cli: main() - selected client port %d\n", client_port);
    #endif
    
    if(init_client(host, host_port, client_port)==INIT_ERROR){
    
        return (ERROR_INIT_CLIENT);
    }
    
    return (RETURN_SUCCESS);
}