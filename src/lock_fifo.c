#include "../include/lock_fifo.h"

void lock_fifo_init(lock_fifo_t* locker){
    
    pthread_cond_init(&locker->cond,NULL);
    pthread_mutex_init(&locker->mutex,NULL);
    
    locker->queue_head = 0;
    locker->queue_tail = 0;
}

void lock_fifo_lock(lock_fifo_t *lock)
{
    unsigned long queue_me;

    pthread_mutex_lock(&lock->mutex);
    queue_me = lock->queue_tail++;
    while (queue_me != lock->queue_head)
    {
        pthread_cond_wait(&lock->cond, &lock->mutex);
    }
    pthread_mutex_unlock(&lock->mutex);
}

void lock_fifo_unlock(lock_fifo_t *lock)
{
    pthread_mutex_lock(&lock->mutex);
    lock->queue_head++;
    pthread_cond_broadcast(&lock->cond);
    pthread_mutex_unlock(&lock->mutex);
}