
#include "../include/ui.h"


void
ui_start(Ui *ui)
{
    initscr();

    ui->ear_box = newwin(LINES - MOUTH_HEIGHT, COLS, 0, 0);
    ui->ear_window = newwin(LINES - MOUTH_HEIGHT - 2, COLS - 2, 1, 1);
    scrollok(ui->ear_window, TRUE);

    ui->mouth_box = newwin(MOUTH_HEIGHT, COLS,
                           LINES - MOUTH_HEIGHT, 0);
    ui->mouth_window = newwin(MOUTH_HEIGHT - 2, COLS - 2,
                              LINES - MOUTH_HEIGHT + 1, 1);

    strcpy(ui->username, DEFAULT_USERNAME);
}

void
ui_finish(Ui *ui)
{
   delwin(ui->ear_box);
   delwin(ui->mouth_box);
   delwin(ui->ear_window);
   delwin(ui->mouth_window);
   endwin();
}

void
ui_listen(Ui *ui, const protocol_t protocol)
{
   char timestamp[9];

   scroll(ui->ear_window);

   strftime(timestamp, 9, "%T", &protocol.date_time);
   mvwprintw(ui->ear_window, LINES - MOUTH_HEIGHT - 3, 0,
             "[%s] <%s> %s", timestamp, protocol.user, protocol.message);
   //mvwprintw(ui->ear_window, 0, 0,
   //          "[%s] <%s> %s", timestamp, protocol.user, protocol.message);

   wmove(ui->mouth_window, 0, 0);

   wrefresh(ui->ear_window);
   wrefresh(ui->mouth_window);
}

void
ui_speak(Ui *ui, protocol_t *protocol)
{
   time_t raw_time;

   mvwgetnstr(ui->mouth_window, 0, 0, protocol->message,
         MAX_PACKET - sizeof(struct tm) - MAX_NAME_LEN);

   time(&raw_time);
   memcpy(&protocol->date_time, localtime(&raw_time), sizeof(protocol->date_time));

   strcpy(protocol->user, ui->username);

   wclear(ui->mouth_window);
   wrefresh(ui->mouth_window);
}

void
ui_show_room(Ui *ui)
{
   box(ui->ear_box, 0, 0);
   wrefresh(ui->ear_box);

   wclear(ui->ear_window);
   wrefresh(ui->ear_window);

   box(ui->mouth_box, 0, 0);
   wrefresh(ui->mouth_box);

   wrefresh(ui->mouth_window);
}

//void
//ui_set_username(Ui *ui)
//{
// mvwgetnstr(ui->mouth_window, 1, 1, ui->username, MAX_NAME_LEN);
//
//}
