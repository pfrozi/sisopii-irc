#include "../include/list_s.h"

void list_init(list_s_t* list){

    list->item = NULL;
    list->next = NULL;
    
    lock_fifo_init(&list->lock);
    
    #if(LIST_S_DEBUG)
    printf("list_s: list_init() at %p\n", list);
    #endif
}
int list_destroy(list_s_t* list){

    #if(LIST_S_DEBUG)
    printf("list_s: list_destroy() at %p\n", list);
    #endif
    
    if(list==NULL){
        return LIST_DESTROY_ERR_NULL;
    }
        
    list_s_t* list_tmp = list;
    
    do{
        
        if(list_tmp->item!=NULL){
            
            #if(LIST_S_DEBUG)
            printf("list_s: list_destroy() at %p\tItem destroy: %p\n", list,list_tmp->item);
            #endif
            
            free(list_tmp->item);
        }
        list_tmp = list_tmp->next;
        
    }while(list_tmp!=NULL);
        
    free(list);
    
    return LIST_DESTROY_SUCCESS;
}

int list_add_head(list_s_t* list, void* item){
    
    
    lock_fifo_lock(&list->lock);
    
        list_s_t* list_new = (list_s_t*)malloc(sizeof(list_s_t));

        list_new->item = item;
        list_new->next = list;

        list = list_new;
    
    lock_fifo_unlock(&list->lock);
    
    return LIST_ADD_SUCCESS;
    
}
int list_add_tail(list_s_t* list, void* item){
    
    #if(LIST_S_DEBUG)
    printf("list_s: list_add_tail(&list = %p, item = %p) run...\n", list, item);
    #endif
    
    lock_fifo_lock(&list->lock);
    
        if(list->item==NULL){

            list->item = item;
            list->next = NULL;

        }
        else{
            #if(LIST_S_DEBUG)
            printf("list_s: list_add_tail(&list = %p, item = %p) first %p\n", list, item,list->item);
            #endif

            list_s_t* list_new = (list_s_t*)malloc(sizeof(list_s_t));

            list_new->item = item;
            list_new->next = NULL;

            list_s_t* list_tmp = list;

            while(list_tmp->next!=NULL){
                list_tmp = list_tmp->next;
            }

            list_tmp->next = list_new;
        }
    lock_fifo_unlock(&list->lock);
    
    return LIST_ADD_SUCCESS;
}


list_s_t* list_remove(list_s_t* list, void* item, int* ret){
    
    #if(LIST_S_DEBUG)
    printf("list_s: list_remove(&list = %p, item = %p) \n", list, item);
    #endif
    
    list_s_t* list_ret  = NULL;
    
    lock_fifo_lock(&list->lock);
    
        if(list==NULL || list->item==NULL){
            if(ret!=NULL) *ret = LIST_REMOVE_LST_EMPTY;
            list_ret = NULL;
        }
        else{
            list_s_t* list_next = list->next;
            list_s_t* list_curr = list;
            list_s_t* list_prev = NULL;


            while(list_curr->item!=item){

                if(list_next==NULL){
                    if(ret!=NULL) *ret = LIST_REMOVE_OUT_RANGE;
                    list_ret = NULL;
                    break;
                }

                list_prev = list_curr;
                list_curr = list_next;
                list_next = list_next->next;
            }
            if(*ret != LIST_REMOVE_OUT_RANGE){
                
                *ret = LIST_REMOVE_SUCCESS;
                
                //free(list_curr->item); make this before come into the function
                free(list_curr);

                if(list_prev!=NULL && list_next!=NULL){

                    list_prev->next = list_next;
                    list_ret =  list_prev;

                }else if(list_prev!=NULL && list_next==NULL){

                    list_ret =  list_prev;

                }else if(list_prev==NULL && list_next!=NULL){

                    list_ret =  list_next;

                }else if(list_prev==NULL && list_next==NULL){

                    list_s_t* new_list = (list_s_t*)malloc(sizeof(list_s_t));
                    list_init(new_list);
                    list_ret =  new_list;
                }
            }
        }
    lock_fifo_unlock(&list->lock);
    
    return list_ret;
}

int list_get_first(list_s_t* list, void* item){
    
    lock_fifo_lock(&list->lock);
    
        if(list!=NULL){
            #if(LIST_S_DEBUG)
            printf("list_s: list_get_first(&list = %p, item = %p) result = %p\n", list, item,list->item);
            #endif
            item = list->item;
        }

    lock_fifo_unlock(&list->lock);
    
    if (list==NULL) return LIST_GET_LST_EMPTY;
    
    return LIST_GET_SUCESS;
}
void* list_get_first2(list_s_t* list){
    
    void* ret = NULL;
    
    lock_fifo_lock(&list->lock);

        if(list!=NULL){
           ret = list->item;
        }

    lock_fifo_unlock(&list->lock);
    
    return ret;
    
}
int list_get_item(list_s_t* list, int index, void* item){

    int i=0;
    
    if(list==NULL){
       return LIST_GET_LST_EMPTY;
    }
    
    list_s_t* list_tmp  = list;
    
    while(i!=index){
        
        if(list_tmp==NULL){
            return LIST_GET_OUT_RANGE;
        }
        
        i++;
        list_tmp = list_tmp->next;
    }
    
    item = list_tmp->item;
    
    return LIST_GET_SUCESS;
}