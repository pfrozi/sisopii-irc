#include "../include/sirc_server.h"

static pthread_t* gravedigger = NULL;
static int        id_user = 0;

static irc_server_t* server = NULL;

int init_server(int port){
    
    #if(SIRC_SERVER_DEBUG)
    printf("sirc_server: init_server(port = %d)\n", port);
    #endif
    
    
    server = (irc_server_t*)malloc(sizeof(irc_server_t));
    
    server->rooms   = (list_s_t*)malloc(sizeof(list_s_t));
    server->postmen = (list_s_t*)malloc(sizeof(list_s_t));
    
    
    
    list_init(server->rooms);
    list_init(server->postmen);
    
    server->sck       = (sck_t*)malloc(sizeof(sck_t));
    
    switch(init_server_sck(server->sck, port)){
     
        case SCK_ERR_CREATE:{
            printf("Erro ao criar socket!\n");
            return INIT_ERROR;
        }
        case SCK_ERR_BIND:{
            printf("Erro ao realizar bind()!\n");
            return INIT_ERROR;
        }
        case SCK_ERR_LISTEN:{
            printf("Erro ao realizar listen()!\n");
            return INIT_ERROR;
        }
    }
    
    #if(SIRC_SERVER_DEBUG)
    printf("sirc_server: init_server_sck(&sck = %p, port = %d) ok!\n", server->sck, port);
    #endif
    
    gravedigger = (pthread_t *)malloc(sizeof(pthread_t));
    pthread_create(gravedigger, NULL,(void*)run_gravedigger, server);
    
    while(1){
        
        init_worker(server);
        usleep(SLEEP_WORKER*1000);
    }
    
    return INIT_SUCCESS;
}

postman_t* create_postman(irc_server_t* server, postman_t* new_postman){
 
    new_postman->user = (irc_user_t *)malloc(sizeof(irc_user_t));
    new_postman->thread_postman = (pthread_t *)malloc(sizeof(pthread_t));
    
    new_postman->status     = POSTMAN_LIVE;
    
    new_postman->irc_server = server;
    new_postman->id_user    = id_user;
    id_user++;
    
    create_user(new_postman);
    
    list_add_tail(server->postmen, new_postman);
    #if(SIRC_SERVER_DEBUG)
    printf("sirc_server: create_postman(&server = %p) first item list %p\n", server, server->postmen->item);
    #endif
    #if(SIRC_SERVER_DEBUG)
    printf("sirc_server: create_postman(&server = %p) postman created ad %p\n", server, new_postman);
    #endif
    
    return new_postman;
}


int create_user(postman_t* postman){
 
    postman->user->id_user = postman->id_user;
    
    #if(SIRC_SERVER_DEBUG)
    printf("sirc_server: create_user(&postman = %p) user created at %p\n", postman, postman->user);
    #endif
    
    return CREATE_USER_SUCCESS;
}

int init_worker(irc_server_t* server){
    
    postman_t* postman = (postman_t *)malloc(sizeof(postman_t));
    postman->sck       = (sck_t*)malloc(sizeof(sck_t));
    
    accept_sck(server->sck, postman->sck);
    
    create_postman(server, postman);
    
    #if(SIRC_SERVER_DEBUG)
    printf("sirc_server: init_worker(&server = %p, &postman = %p) socket accepted on %d!\n", server, postman, postman->sck->socket);
    #endif
    
    pthread_create(postman->thread_postman, NULL,(void*)run_postman, postman);
}

void *run_postman(postman_t* postman){
    
    char buff[MAX_PACKET];
    
    #if(SIRC_SERVER_DEBUG)
    printf("sirc_server: *run_postman(&postman = %p) thread is running!\n", postman);
    #endif
    
    while(postman->status == POSTMAN_LIVE){
        
        memset(buff, '\0', sizeof(buff));
        
        if ((recv(postman->sck->socket, buff, MAX_PACKET, 0)) <= 0)
        {
            close(postman->sck->socket);
            postman->status = POSTMAN_DEAD_ERR_REC;
        }
        else{
            
            protocol_t proto;
            memcpy(&proto, buff, MAX_PACKET);
            
            #if(SIRC_SERVER_DEBUG)
            char buff_time[25];
            strftime(buff_time,25,"%d/%m/%Y %X",&proto.date_time);
            printf("sirc_server: *run_postman(&postman = %p) Mensagem recebida!\n", postman);
            printf("\t\tprotocol = {date_time = %s\n\t\t, user = '%s'\n\t\t, message = '%s'}\n", buff_time, proto.user, proto.message);
            #endif
            
            strncpy(postman->user->nick, proto.user, MAX_NAME_LEN_BUFF);
            
            process_message(postman, proto);
            
        }
    }
}

void process_message(postman_t* postman, protocol_t proto){
    
    #if(SIRC_SERVER_DEBUG)
    char buff_time[25];
    strftime(buff_time,25,"%d/%m/%Y %X",&proto.date_time);
    printf("sirc_server: postman(&postman = %p\n\t\t, protocol = {date_time = %s\n\t\t, user = '%s'\n\t\t,message = '%s'}) Process message...\n", postman, buff_time, proto.user, proto.message);
    #endif
    
    if(proto.message[0]==SIRC_COMMAND_IDENT){
        
        #if(SIRC_SERVER_DEBUG)
        strftime(buff_time,25,"%d/%m/%Y %X",&proto.date_time);
        printf("sirc_server: process_message(&postman = %p\n\t\t, protocol = {date_time = %s\n\t\t, user = '%s'\n\t\t,message = '%s'}) Message is a command!\n", postman, buff_time, proto.user, proto.message);
        #endif
        
        read_command(postman, proto.message);
    }
    else {
        
        #if(SIRC_SERVER_DEBUG)
        strftime(buff_time,25,"%d/%m/%Y %X",&proto.date_time);
        printf("sirc_server: process_message(&postman = %p\n\t\t, protocol = {date_time = %s\n\t\t, user = '%s'\n\t\t,message = '%s'}) Message is broadcast!\n", postman, buff_time, proto.user, proto.message);
        #endif
        
        comm_send_broadcast_room(postman, proto);
    }
    
}

void server_send(postman_t* postman, protocol_t proto){
    
    #if(SIRC_SERVER_DEBUG)
    char buff_time[25];
    strftime(buff_time,25,"%d/%m/%Y %X",&proto.date_time);
    printf("sirc_server: server_send(&postman = %p\n\t\t, protocol = {date_time = %s\n\t\t, user = '%s'\n\t\t,message = '%s'}) Send message...\n", postman, buff_time, proto.user, proto.message);
    #endif
    
    if(send_message(postman->sck->socket, proto) == SEND_ERROR){
        postman->status = POSTMAN_DEAD_ERR_SND;
    }
    
    #if(SIRC_SERVER_DEBUG)
    strftime(buff_time,25,"%d/%m/%Y %X",&proto.date_time);
    printf("sirc_server: server_send(&postman = %p\n\t\t, protocol = {date_time = %s\n\t\t, user = '%s'\n\t\t,message = '%s'}) Send ok!\n", postman, buff_time, proto.user, proto.message);
    #endif
}
    
void *run_gravedigger(irc_server_t* server){

    #if(SIRC_SERVER_DEBUG)
    printf("sirc_server: *run_gravedigger(&server = %p) Init gravedigger...\n", server);
    #endif
    
    while(1){
        
        usleep(SLEEP_GRAVEDIGGER*1000);
        
        list_s_t* l = server->postmen;
        
        while(l!=NULL){
            
            postman_t* p = list_get_first2(l);
            
            if(p== NULL){
                
                break;
            }else{
                
                if(p->status!=POSTMAN_LIVE){
                    
                    #if(SIRC_SERVER_DEBUG)
                    printf("sirc_server: *run_gravedigger(&server = %p) Bury postman %p...\n", server,p);
                    #endif
                    
                    int ret;
                    
                    server->postmen = list_remove(server->postmen, p, &ret);
                    
                    if(p->sck->socket>=0){
                        close(p->sck->socket);
                    }
                    if(p->sck!=NULL){
                        free(p->sck);
                    }
                    if(p->thread_postman!=NULL){
                        free(p->thread_postman);
                    }
                    
                    free(p);
                    
                    #if(SIRC_SERVER_DEBUG)
                    printf("sirc_server: *run_gravedigger(&server = %p) Postman Buried!\n", server);
                    #endif
                }
            }
            l = l->next;
        }
        
    }
}
    
int create_room(irc_server_t* s, char* name){
    
    irc_room_t* room = (irc_room_t*)malloc(sizeof(irc_room_t)); 
    
    strncpy(room->name, name, MAX_NAME_LEN_BUFF);
    
    list_add_tail(s->rooms, room);
    
}
    
/*
#########################################
    COMMANDS
#########################################
*/    
    
int read_command(postman_t* postman, char* message){
    
    #if(SIRC_SERVER_DEBUG)
    printf("read_command (commands): read_command(&postman = %p, message = '%s')\n", postman, message);
    #endif
    
    char *command   = malloc(sizeof(char) * MAX_ENTRY);
    char *entry     = malloc(sizeof(char) * MESSAGE_BUFF_SIZE);
    
    char *pch;
    char *string_to_compare;
    
    if(strlen(message) > MAX_ENTRY*2){
        return ERR_READ_COMM_ENTRY_TOO_BIG;
    }
    
    if(message[0]==SIRC_COMMAND_IDENT){
        
        #if(SIRC_SERVER_DEBUG)
        printf("read_command (commands): read_command(&postman = %p, message = '%s')Is command!\n", postman, message);
        #endif
        
        string_to_compare = malloc(sizeof(char) * MESSAGE_BUFF_SIZE);
        
        strcpy(string_to_compare, message);                   
        
        #if(SIRC_SERVER_DEBUG)
        printf("read_command (commands): read_command(&postman = %p, message = '%s')Is memove ok! string_to_compare = '%s'\n", postman, message, string_to_compare);
        #endif
        if (string_to_compare[0] != '\n') {
           //remove the first char "/"
           memmove(string_to_compare, string_to_compare+1, strlen(string_to_compare));
            
            #if(SIRC_SERVER_DEBUG)
            printf("read_command (commands): read_command(&postman = %p, message = '%s')Is memove ok! new str = '%s'\n", postman, message, string_to_compare);
            #endif
        }
        else{
           return ERR_READ_COMM_INVALID;
        }	   

        if(strncmp(string_to_compare, SIRC_COMM_PING, MAX_ENTRY) == 0){
            return comm_ping(postman);
            

        }else if(strncmp(string_to_compare, SIRC_COMM_EXIT, MAX_ENTRY) == 0){
            return comm_exit(postman);
            
        }else if(strncmp(string_to_compare, SIRC_COMM_LEAVE_ROOM, MAX_ENTRY) == 0){
            return comm_leave_room(postman, NULL);
            
        }else if(strncmp(string_to_compare, SIRC_COMM_LIST_USERS, MAX_ENTRY) == 0){
            return comm_list_users(postman);
            
        }else if(strncmp(string_to_compare, SIRC_COMM_LIST_ROOMS, MAX_ENTRY) == 0){
            return comm_list_roms(postman);
        }
        
        //slice string : command + entry
        pch = strtok (string_to_compare," ");
        if (pch != NULL)
        {
            strcpy(command, pch);
            pch = strtok (NULL, " ");
            if(pch != NULL){
                strcpy(entry, pch);
            }
            else
                return ERR_READ_COMM_INVALID;
        }
        else {
            return ERR_READ_COMM_INVALID;
        }

        #if(SIRC_SERVER_DEBUG)
        printf("read_command (commands): read_command(&postman = %p, message = '%s') Command selected: '%s', entry: '%s'\n", postman, message,command,entry);
        #endif

        //"switch/case"
        if(strcmp(command, SIRC_COMM_CREATE_USER) == 0){
            comm_create_user(postman, entry);
        }
        else if(strcmp(command, SIRC_COMM_ALL) == 0){
            comm_send_broadcast(postman, create_message(postman->user->nick, entry));
        }
        else if(strcmp(command, SIRC_COMM_CREATE_ROOM) == 0){
            comm_create_room(postman, entry);
        }
        else if(strcmp(command, SIRC_COMM_JOIN_ROOM) == 0){
            comm_join_room(postman, entry);
        }
        else if(strcmp(command, SIRC_COMM_LEAVE_ROOM) == 0){
            comm_leave_room(postman, entry);
        }
        else if(strcmp(command, SIRC_COMM_ALTER_NICK) == 0){
            comm_alter_nick(postman, entry);
        }
        else {
                return ERR_READ_COMM_INVALID;
        }
    }
    else {
        return ERR_READ_COMM_INVALID;
    }
    free(entry);
    free(command);
    free(string_to_compare);
    return READ_COMM_SUCCESS;

}
int comm_exit(postman_t* postman){
    
    char msg[MESSAGE_BUFF_SIZE];
    
    snprintf(msg, MESSAGE_BUFF_SIZE, "Usuario %s desconectou!", postman->user->nick);
    comm_send_broadcast(postman, create_message(NICK_SERVER, msg));
    
    postman->status = POSTMAN_DEAD_EXIT;
    
    return EXIT_SUCCESS;
}
int comm_create_user(postman_t* postman, char* nick){
    
    
    return CREATE_USER_SUCCESS;
}

int comm_create_room(postman_t* postman, char* name){
    
    char msg[MESSAGE_BUFF_SIZE];
        
    list_s_t* l = postman->irc_server->rooms;
    
    while(l!=NULL){
        
        irc_room_t* r = list_get_first2(l);

        if(r==NULL){
            
            break;
        }else{
            
            if(strncmp(name,r->name,MAX_NAME_LEN_BUFF)==0){
                
                snprintf(msg, MESSAGE_BUFF_SIZE, "A sala %s jah existe!", name);
                
                if(send_message(postman->sck->socket, create_message(NICK_SERVER, msg))==SEND_ERROR){
                    printf("Erro ao enviar ping!\n");
                    return ERR_COMM_SEND;
                }
                else{
                    return ERR_CREATE_ROOM_EXISTS;   
                }
            }
        }
        
        l = l->next;
    }
    
    create_room(postman->irc_server, name);
    snprintf(msg, MESSAGE_BUFF_SIZE, "A sala %s foi criada com sucesso!", name);
    if(send_message(postman->sck->socket, create_message(NICK_SERVER, msg))==SEND_ERROR){
        printf("Erro ao enviar ping!\n");
        return ERR_SEND_BROAD;
    }
    
    return CREATE_ROOM_SUCESS;
}

int comm_join_room(postman_t* postman, char* name){
    
    #if(SIRC_SERVER_DEBUG)
    printf("sirc_server (commands): comm_join_room(&postman = %p, room = '%s')\n", postman, name);
    #endif
    
    list_s_t* l = postman->irc_server->rooms;
    
    while(l!=NULL){
        
        irc_room_t* r = list_get_first2(l);

        if(r==NULL){
            
            break;
        }else{
            
            if(strncmp(name,r->name,MAX_NAME_LEN_BUFF)==0){
                
                comm_leave_room(postman, postman->user->room->name);
                
                postman->user->room = r;
                
                char msg[MESSAGE_BUFF_SIZE];
                snprintf(msg, MESSAGE_BUFF_SIZE, "%s entrou na sala!", postman->user->nick);
                
                if(comm_send_broadcast_room(postman, create_message(NICK_SERVER, msg))!=SEND_ERROR){
                    
                    return JOIN_SUCESS;
                }
                else{
                    return ERR_COMM_SEND;   
                }
            }
        }
        
        l = l->next;
    }
    
    if(send_message(postman->sck->socket, create_message(NICK_SERVER,"A sala selecionada nao existe!"))==SEND_ERROR){
        printf("Erro ao enviar ping!\n");
        return ERR_SEND_BROAD;
    }
    
    return JOIN_SUCESS;
}

int comm_leave_room(postman_t* postman, char* name){
    
    #if(SIRC_SERVER_DEBUG)
    printf("sirc_server (commands): comm_leave_room(&postman = %p, room = '%s')\n", postman, name);
    #endif
    
    char msg[MESSAGE_BUFF_SIZE];
    snprintf(msg, MESSAGE_BUFF_SIZE, "Usuario %s saiu da sala!", postman->user->nick);
    
    if(comm_send_broadcast_room(postman, create_message(NICK_SERVER, msg))!=SEND_ERROR){
        postman->user->room = NULL;
        return LEAVE_SUCESS;
    }
    else{
        return ERR_LEAVE_ERROR;   
    }
    
    return LEAVE_SUCESS;
}

int comm_alter_nick(postman_t* postman, char* nick){
    
    char old_nick[MAX_NAME_LEN_BUFF];  
    char msg[MESSAGE_BUFF_SIZE];
    
    strncpy(old_nick, postman->user->nick, MAX_NAME_LEN_BUFF);
    
    memset(postman->user->nick, '\0', MAX_NAME_LEN_BUFF);
    strncpy(postman->user->nick, nick, MAX_NAME_LEN_BUFF);
    
    if(send_message(postman->sck->socket
                    , create_message_comm(NICK_SERVER, nick, S_TO_C_COMM_NICK)) ==SEND_ERROR){
        
        printf("Erro ao enviar confirmacao de troca de nick!\n");
        return ERR_ALTER_NICK;
    }
    
    snprintf(msg, MESSAGE_BUFF_SIZE, "Usuario %s trocou o nick para %s!", old_nick, nick);
    
    comm_send_broadcast_room(postman, create_message(NICK_SERVER, msg));
    
    return ALTER_NICK_SUCESS;
}

int comm_send_broadcast(postman_t* postman, protocol_t proto){
    
    #if(SIRC_SERVER_DEBUG)
    char buff_time[25];
    strftime(buff_time,25,"%d/%m/%Y %X",&proto.date_time);
    printf("sirc_server (commands): comm_send_broadcast(&postman = %p\n\t\t, protocol = {date_time = %s\n\t\t, user = '%s'\n\t\t,message = '%s'}) Message is broadcast\n\t\t, &server = %p\n\t\t, &postmen = %p\n", postman, buff_time, proto.user, proto.message,postman->irc_server,postman->irc_server->postmen);
    #endif
    
    list_s_t* l = postman->irc_server->postmen;
    
    while(l!=NULL){
        
        postman_t* p = list_get_first2(l);

        if(p==NULL){
            
            #if(SIRC_SERVER_DEBUG)
            printf("sirc_server (commands): comm_send_broadcast(...) List is empty, l = %p, it = %p\n",l,p);
            #endif
            break;
        }else{

            #if(SIRC_SERVER_DEBUG)
            printf("sirc_server (commands): comm_send_broadcast(...) postman get, l = %p, &postman = %p\n",l,p);
            #endif
            
            if(p->status == POSTMAN_LIVE){
                
                #if(SIRC_SERVER_DEBUG)
                strftime(buff_time,25,"%d/%m/%Y %X",&proto.date_time);
                printf("sirc_server (commands): comm_send_broadcast(&postman = %p\n\t\t, protocol = {date_time = %s\n\t\t, user = '%s'\n\t\t,message = '%s'}) Message is broadcasting!\n", postman, buff_time, proto.user, proto.message);
                printf("\t\t Send message to socket (sck = %d, &postman = %p)", p->sck->socket, p);
                #endif
                
                if(send_message(p->sck->socket, proto)==SEND_ERROR){
                    printf("Erro ao enviar ping!\n");
                    return ERR_SEND_BROAD;
                }
            }
        }
        
        l = l->next;
    }
    
    return SEND_BROAD_SUCESS;
}

int comm_send_broadcast_room(postman_t* postman, protocol_t proto){
    
    #if(SIRC_SERVER_DEBUG)
    char buff_time[25];
    strftime(buff_time,25,"%d/%m/%Y %X",&proto.date_time);
    printf("sirc_server (commands): comm_send_broadcast_room(&postman = %p\n\t\t, protocol = {date_time = %s\n\t\t, user = '%s'\n\t\t,message = '%s'}) Message is broadcast\n\t\t, &server = %p\n\t\t, &postmen = %p\n", postman, buff_time, proto.user, proto.message,postman->irc_server,postman->irc_server->postmen);
    #endif
    
    if(postman->user->room==NULL){
        if(send_message(postman->sck->socket, create_message(NICK_SERVER, "Voce precisa entrar em uma sala para se comunicar, ou entao crie a sua!"))==SEND_ERROR){
            printf("Erro ao enviar mensagem em broadcast para uma sala!\n");
            return ERR_SEND_BROAD;
        }
        return SEND_BROAD_SUCESS;
    }
    
    list_s_t* l = postman->irc_server->postmen;
    
    while(l!=NULL){
        
        postman_t* p = list_get_first2(l);

        if(p==NULL){
            
            #if(SIRC_SERVER_DEBUG)
            printf("sirc_server (commands): comm_send_broadcast_room(...) List is empty, l = %p, it = %p\n",l,p);
            #endif
            break;
        }else{

            #if(SIRC_SERVER_DEBUG)
            printf("sirc_server (commands): comm_send_broadcast_room(...) postman get, l = %p, &postman = %p\n",l,p);
            #endif
            
            if(p->status        == POSTMAN_LIVE 
               && p->user->room != NULL 
               && (strcmp(postman->user->room->name, p->user->room->name)==0)){
                
                #if(SIRC_SERVER_DEBUG)
                strftime(buff_time,25,"%d/%m/%Y %X",&proto.date_time);
                printf("sirc_server (commands): comm_send_broadcast_room(&postman = %p\n\t\t, protocol = {date_time = %s\n\t\t, user = '%s'\n\t\t,message = '%s'}) Message is broadcasting!\n", postman, buff_time, proto.user, proto.message);
                printf("\t\t Send message to socket (sck = %d, &postman = %p)", p->sck->socket, p);
                #endif
                
                if(send_message(p->sck->socket, proto)==SEND_ERROR){
                    printf("Erro ao enviar mensagem em broadcast para uma sala!\n");
                    return ERR_SEND_BROAD;
                }
            }
        }
        
        l = l->next;
    }
    
    return SEND_BROAD_SUCESS;
}

int comm_ping(postman_t* postman){
    
    #if(SIRC_SERVER_DEBUG)
    printf("sirc_server (commands): comm_ping(&postman = %p) User is %p\n", postman, postman->user);
    #endif
    
    char msg[MAX_NAME_LEN_BUFF+32];
    
    snprintf(msg, MAX_NAME_LEN_BUFF+32, "Pong to %s!", postman->user->nick);
    
    protocol_t  p = create_message(NICK_SERVER, msg);
    
    comm_send_broadcast(postman, p);
}

int comm_list_users(postman_t* postman){
    
    #if(SIRC_SERVER_DEBUG)
    printf("comm_list_users (commands): comm_ping(&postman = %p) User is %p\n", postman, postman->user);
    #endif
    int  size_item = 200;
    char msg[MESSAGE_BUFF_SIZE];
    char item_list[size_item];
    
    memset(msg, '\0', MESSAGE_BUFF_SIZE);
    msg[0] = '\n';
    
    snprintf(item_list, size_item, SIRC_FORMAT_LST, "Nick", "Sala");
    strncat(msg, item_list, MESSAGE_BUFF_SIZE);
    
    list_s_t* l = postman->irc_server->postmen;
    
    while(l!=NULL){
        
        postman_t* p = list_get_first2(l);

        if(p==NULL){
            
            #if(SIRC_SERVER_DEBUG)
            printf("sirc_server (commands): comm_list_users(...) List is empty, l = %p, it = %p\n",l,p);
            #endif
            break;
        }else{
            
            if(p->status == POSTMAN_LIVE){
                
                memset(item_list, '\0', size_item);
                if(postman->user->room != NULL){
                    snprintf(item_list, size_item, SIRC_FORMAT_LST, p->user->nick, p->user->room->name);
                }
                else {
                    snprintf(item_list, size_item, SIRC_FORMAT_LST, p->user->nick, "<Nenhuma>");
                }
                
                strncat(msg, item_list, MESSAGE_BUFF_SIZE);
            }
        }
        
        l = l->next;
    }
    
    protocol_t  proto = create_message(NICK_SERVER, msg);
    
    if(send_message(postman->sck->socket, proto)==SEND_ERROR){
        printf("Erro ao enviar mensagem para usuario!\n");
        return ERR_SEND_LISTU;
    }
    return SEND_LISTU_OK;
    
}
int comm_list_roms(postman_t* postman){
 
    #if(SIRC_SERVER_DEBUG)
    printf("comm_list_roms (commands): comm_ping(&postman = %p) User is %p\n", postman, postman->user);
    #endif
    int  size_item = 200;
    char msg[MESSAGE_BUFF_SIZE];
    char item_list[size_item];
        
    memset(msg, '\0', MESSAGE_BUFF_SIZE);
    msg[0] = '\n';
    
    snprintf(item_list, size_item, SIRC_FORMAT_LST, "Sala", "Quant");
    strncat(msg, item_list, MESSAGE_BUFF_SIZE);
    
    list_s_t* l = postman->irc_server->rooms;
    
    while(l!=NULL){
        
        irc_room_t* r = list_get_first2(l);

        if(r==NULL){
            
            break;
        }else{

            int n_users = 0;
            
            list_s_t* postmen = postman->irc_server->postmen;
    
            while(postmen!=NULL){

                postman_t* p = list_get_first2(postmen);

                if(p==NULL){
                    break;
                }else{

                    if(p->status == POSTMAN_LIVE && p->user->room!=NULL && strcmp(p->user->room->name, r->name)==0) n_users++;
                    
                }

                postmen = postmen->next;
            }
            memset(item_list, '\0', size_item);
            snprintf(item_list, size_item, SIRC_FORMAT_LST2, r->name, n_users);
            strncat(msg, item_list, MESSAGE_BUFF_SIZE);
        }
        
        l = l->next;
    }
    
    protocol_t  proto = create_message(NICK_SERVER, msg);
    
    if(send_message(postman->sck->socket, proto)==SEND_ERROR){
        printf("Erro ao enviar mensagem para usuario!\n");
        return ERR_SEND_LISTR;
    }
    return SEND_LISTR_OK;
}