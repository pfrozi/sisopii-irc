#include "../include/main_srv.h"

void help(){

    printf("\n _____ _____ _____  ___________ _____ _____ _____         ");
    printf("\n/  ___|_   _/  ___||  _  | ___ \\_   _|_   _|_   _|        ");
    printf("\n\\ `--.  | | \\ `--. | | | | |_/ / | |   | |   | | _ __ ___ ");
    printf("\n `--. \\ | |  `--. \\| | | |  __/  | |   | |   | || '__/ __|");
    printf("\n/\\__/ /_| |_/\\__/ /\\ \\_/ / |    _| |_ _| |_ _| || | | (__ ");
    printf("\n\\____/ \\___/\\____/  \\___/\\_|    \\___/ \\___/ \\___/_|  \\___|");
    printf("\n");
    printf("\n");
    printf("\nTrabalho de Sistemas Operacionais II");
    printf("\nAplicativo de Chat utilizando sockets - SISOPIIirc");
    printf("\n");
    printf("\nsirc-server");
    printf("\n\t-h\t, Ajuda");
    printf("\n\t-p\t, Numero da porta do servidor");
    printf("\n");
    printf("\nsirc-client");
    printf("\n\t-h\t, Ajuda");
    printf("\n\t-s\t, IP do servidor");
    printf("\n\t-p\t, Porta do servidor");
    printf("\n\t-c\t, Porta do cliente");
    printf("\n");
    printf("\n");
}

int main(int argc, char **argv) {
    
    #if(MAIN_SRV_DEBUG)
    printf("main_srv: main() init\n");
    #endif
    int port_number = -1;
    
    if (argc < ARGS_MAX || (argc==1 && !(strcmp(argv[1],ARGS_HELP)==0) )) {
        
        printf("Argumentos invalidos!\n");
        
        help();
        return (ERROR_INVALID_ARGS);

    } else {
        
        int i;
        
        for (i = 1; i < argc; i+=2) {

            if (i + 1 != argc)
            {
                char* arg = argv[i+1];

                if (strcmp(argv[i],ARGS_HELP) == 0) {

                    help();
                    return (RETURN_NOTHING);

                } else if (strcmp(argv[i], ARGS_PORT_NUMBER)==0) {

                    port_number = atoi(arg);

                } else {
                    
                    printf("Argumentos invalidos!\n");
                    return (ERROR_INVALID_ARGS);

                }
            }
        }
    }

    
    if(!(port_number>=0 && port_number<=65535)){
    
        printf("ERR: %s (port %d)\n", ERR_PORT_NUMBER, port_number);
        return(ERROR_INVALID_PORT);
        
    } else if(port_number>=0 && port_number<=1023){
    
        printf("ERR: %s (port %d)\n", ERR_PORT_RESERVED, port_number);
        return(ERROR_INVALID_PORT);
        
    }    
    
    #if(MAIN_SRV_DEBUG)
    printf("main_srv: main() - selected port %d\n", port_number);
    #endif
    
    if(init_server(port_number)==INIT_ERROR){
        
        return (ERROR_INIT_SERVER);
    }
    
    return (RETURN_SUCCESS);
}