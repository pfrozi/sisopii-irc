#include "../include/sirc_util.h"

int init_server_sck(sck_t* sck, int port){
    
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: init_server_sck(&sck = %p, port = %d)\n", sck, port);
    #endif
    
    sck->port                 = port;
    
    sck->addr.sin_family      = AF_INET; 
    sck->addr.sin_addr.s_addr = htonl(INADDR_ANY);      // receive any iP
    sck->addr.sin_port        = htons(sck->port);
    
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: init_server_sck(&sck = %p, port = %d) - addr defined!\n", sck, port);
    #endif
    
    // create socket: 
    //      AF_INET - IPv4 Internet protocols; 
    //      SOCK_STREAM - sequenced, reliable, two-way, connection-based byte streams;
    sck->socket = socket(AF_INET, SOCK_STREAM, 0);
    if (sck->socket == INVALID_SOCKET){
        close(sck->socket);
        return SCK_ERR_CREATE;
    }
    
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: init_server_sck(&sck = %p, port = %d) - socket create at %d!\n", sck, port, sck->socket);
    #endif
    
    // bind socket
    if ((bind(sck->socket, (const struct sockaddr *)&sck->addr, sizeof(struct sockaddr))) != 0){
        close(sck->socket);
        return SCK_ERR_BIND;   
    }
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: init_server_sck(&sck = %p, port = %d) - bind() ok!\n", sck, port);
    #endif
    
    // listen state
    if((listen(sck->socket, CONCURRENT_CONNECTIONS)) != 0){
        close(sck->socket);
        return SCK_ERR_LISTEN;
    }
    
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: init_server_sck(&sck = %p, port = %d) - listen() ok!\n", sck, port);
    #endif
    
    // ready to accept!
    return SCK_INIT_SUCCESS;
    
        
}

int init_client_sck(sck_t* sck, int port){
    
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: init_client_sck(&sck = %p, port = %d)\n", sck, port);
    #endif
    
    sck->port                 = port;
    
    sck->addr.sin_family      = AF_INET; 
    sck->addr.sin_addr.s_addr = htonl(INADDR_ANY);      // receive any iP
    sck->addr.sin_port        = htons(sck->port);
    
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: init_client_sck(&sck = %p, port = %d) - addr defined!\n", sck, port);
    #endif
    
    // create socket: 
    //      AF_INET - IPv4 Internet protocols; 
    //      SOCK_STREAM - sequenced, reliable, two-way, connection-based byte streams;
    sck->socket = socket(AF_INET, SOCK_STREAM, 0);
    if (sck->socket == INVALID_SOCKET){
        close(sck->socket);
        return SCK_ERR_CREATE;
    }
    
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: init_server_sck(&sck = %p, port = %d) - socket create at %d!\n", sck, port, sck->socket);
    #endif
    
    // bind socket
    if ((bind(sck->socket, (const struct sockaddr *)&sck->addr, sizeof(sck->addr))) != 0){
        close(sck->socket);
        return SCK_ERR_BIND;   
    }
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: init_server_sck(&sck = %p, port = %d) - bind() ok!\n", sck, port);
    #endif
    
    // ready to connect!
    return SCK_INIT_SUCCESS;
    
        
}

int accept_sck(sck_t* sck, sck_t* sck_ret){
   
    int addr_len         = sizeof(sck_ret->addr);
    
    if((sck_ret->socket = accept(sck->socket, (struct sockaddr *)&sck_ret->addr, &addr_len)) <= 0){
	    return SCK_ACCEPT_ERROR;
    }
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: accept_sck(&client_sck_t = %p, sck_ret = %p)Accept!\n", sck, sck_ret);
    #endif
    
    return SCK_ACCEPT_SUCCESS;
}

int    init_client_to_server(client_sck_t* sck_client, char* host, int port){
 
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: init_client_to_server(&client_sck_t = %p, host = %s, port = %d)\n", sck_client, host, port);
    #endif
    
    sck_client->addr_server.sin_family      = AF_INET;
    sck_client->addr_server.sin_addr.s_addr = inet_addr(host);
    sck_client->addr_server.sin_port        = htons(port);
    
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: init_client_to_server(&client_sck_t = %p, host = %s, port = %d) addr host defined!\n", sck_client, host, port);
    #endif
    
    int addr_len         = sizeof(sck_client->addr_server);
    
    // connect client socket with server socket
    if(connect(sck_client->sck->socket, (struct sockaddr*)&sck_client->addr_server, addr_len) != 0)
    {
        close(sck_client->sck->socket);
        return SCK_ERR_CONNECT;
    }
    
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: init_client_to_server(&client_sck_t = %p, host = %s, port = %d) connect ok!\n", sck_client, host, port);
    #endif
    
}

protocol_t create_message(char* user, char* message){
    
    return 
        create_message_comm(user, message, S_TO_C_COMM_NULL);
}

protocol_t create_message_comm(char* user, char* message, int command){
    
    #if(SIRC_UTIL_DEBUG)
    printf("sirc_util: create_message(user = %s, message = %s) Criando mensagem ...\n", user, message);
    #endif
    
    protocol_t proto;
    
    memset(proto.user, '\0', sizeof(proto.user));
    memset(proto.message, '\0', sizeof(proto.message));
    
    time_t rawtime;
    time(&rawtime);
    
    memcpy(&proto.date_time, localtime(&rawtime), sizeof(proto.date_time));
    strncpy(proto.user, user, MAX_NAME_LEN_BUFF);
    strncpy(proto.message, message, MESSAGE_BUFF_SIZE);
    
    #if(SIRC_UTIL_DEBUG)
    char buff_time[25];
    strftime(buff_time,25,"%d/%m/%Y %X",&proto.date_time);
    printf("sirc_util: create_message(user = %s, message = %s) Mensagem criada!\n", user, message);
    printf("\t\tprotocol = {date_time = %s\n\t\t, user = '%s'\n\t\t, message = '%s'}\n", buff_time, user, message);
    #endif
    
    proto.s_to_c_command = command;
    
    return proto;
}

int send_message(SOCKET socket, protocol_t message){
 
    if ((send(socket, (const char *)&message, MAX_PACKET,0)) <= 0)
    {
        return (SEND_ERROR);
    }
    
    return (SEND_OK);
}


int receive_message(SOCKET socket, protocol_t *message){
 
    if ((recv(socket, (char *)message, MAX_PACKET,0)) <= 0)
    {
        return (RECV_ERROR);
    }

    return (RECV_OK);
}